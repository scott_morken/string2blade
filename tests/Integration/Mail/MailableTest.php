<?php

declare(strict_types=1);

namespace Tests\Smorken\String2Blade\Integration\Mail;

use PHPUnit\Framework\Attributes\Test;
use Smorken\String2Blade\Mail\Mailable;
use Tests\Smorken\String2Blade\BaseTestCase;
use Tests\Smorken\String2Blade\Stubs\MarkdownMailableStub;
use Tests\Smorken\String2Blade\Stubs\ViewMailableStub;

/**
 * can only run one at a time.  problem with namespaced path hints
 */
class MailableTest extends BaseTestCase
{
    #[Test]
    public function it_is_an_exception_with_an_empty_view_property(): void
    {
        $sut = new Mailable;
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid view');
        $sut->assertSeeInHtml('', false);
    }

    public function test_mailable_handles_markdown_string(): void
    {
        $sut = new MarkdownMailableStub('<x-mail::message>foo bar</x-mail::message>');
        $sut->assertHasSubject('Test');
        $sut->assertFrom('test@example.edu');
        $sut->assertSeeInHtml('>foo bar</p>', false);
    }

    public function test_mailable_handles_view_string(): void
    {
        $sut = new ViewMailableStub('<div>foo bar</div>');
        $sut->assertHasSubject('Test');
        $sut->assertFrom('test@example.edu');
        $sut->assertSeeInHtml('<div>foo bar</div>', false);
    }
}
