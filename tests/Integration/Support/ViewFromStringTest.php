<?php

declare(strict_types=1);

namespace Tests\Smorken\String2Blade\Integration\Support;

use Illuminate\Contracts\View\View;
use Smorken\String2Blade\Contracts\Support\ViewFromString;
use Tests\Smorken\String2Blade\BaseTestCase;

class ViewFromStringTest extends BaseTestCase
{
    public function test_it_can_create_a_view_and_add_data_via_with(): void
    {
        $sut = $this->app[ViewFromString::class];
        $view = $sut->view('<div>Hello {{ $name }}</div>');
        $view->with('name', 'Foo');
        $this->assertInstanceOf(View::class, $view);
        $this->assertEquals('<div>Hello Foo</div>', (string) $view);
    }

    public function test_it_can_create_a_view_from_a_string(): void
    {
        $sut = $this->app[ViewFromString::class];
        $view = $sut->view('<div>Hello {{ $name }}</div>', ['name' => 'Foo']);
        $this->assertInstanceOf(View::class, $view);
        $this->assertEquals('<div>Hello Foo</div>', (string) $view);
    }
}
