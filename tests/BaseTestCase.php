<?php

namespace Tests\Smorken\String2Blade;

use Orchestra\Testbench\TestCase;
use Smorken\String2Blade\ServiceProvider;

abstract class BaseTestCase extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [
            ServiceProvider::class,
        ];
    }
}
