<?php

namespace Tests\Smorken\String2Blade\Unit\Concerns;

use PHPUnit\Framework\TestCase;
use Smorken\String2Blade\Concerns\IsString2BladeTrait;

class IsString2BladeTest extends TestCase
{
    use IsString2BladeTrait;

    public function testSpaceIsTrue(): void
    {
        $this->assertTrue($this->isString2Blade('hello world'));
    }

    public function testBladeTemplateStringIsTrue(): void
    {
        $this->assertTrue($this->isString2Blade('hello {{ $world }}'));
    }

    public function testViewNameNoDotsIsFalse(): void
    {
        $this->assertFalse($this->isString2Blade('hello'));
    }

    public function testViewNameNamespacedIsFalse(): void
    {
        $this->assertFalse($this->isString2Blade('namespace::hello.admin.index'));
        $this->assertFalse($this->isString2Blade('myviews/namespace::hello.admin.index'));
    }

    public function testViewNameWithDotsIsFalse(): void
    {
        $this->assertFalse($this->isString2Blade('hello.admin.index'));
    }

    public function testSimplePathIsFalse(): void
    {
        $this->assertFalse($this->isString2Blade('/app/test/index.blade.php'));
    }

    public function testPathIsFalse(): void
    {
        $this->assertFalse($this->isString2Blade('/app/test-one/foo/index.blade.php'));
    }
}
