<?php

declare(strict_types=1);

namespace Tests\Smorken\String2Blade\Stubs;

use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Smorken\String2Blade\Mail\Mailable;

class MarkdownMailableStub extends Mailable
{
    public function __construct(
        protected string $template
    ) {}

    public function content(): Content
    {
        return new Content(
            markdown: $this->template
        );
    }

    public function envelope(): Envelope
    {
        return new Envelope(from: 'test@example.edu', subject: 'Test');
    }
}
