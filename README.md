## String to blade compiler for Laravel

Provides the ability for Laravel to handle raw strings as blade templates.

Per the norm, you are responsible for what is in the template string!  User provided input is probably a terrible idea.

The 'string' will be cached using a sha1 of the value so modifying the string value should result in laravel creating a
new cached file.

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Installation

It should register itself automatically. If not, add to your `config/app.php`:

    ...
    'providers' => [
            Smorken\String2Blade\ServiceProvider::class,
            Smorken\String2Blade\MailerServiceProvider::class,
    ...

### String2Blade Use

* Contract

```php
// create view factory from DI
$view = App::make(\Smorken\String2Blade\Contracts\View\Factory::class);
$view->make('hello {{ $world }}!', ['world' => 'Earth']);
```

* Facade

```php
use Smorken\String2Blade\Facades\String2Blade;

String2Blade::make('hello {{ $world }}')->with('world', 'Earth');
```

### Mailable Use

* Blade template string

```php
use Smorken\String2Blade\Mail\Mailable;

class TestMailable extends Mailable
{

    public function build(): void
    {
        $this->to('fiz@example.com')
             ->from('buz@example.com')
             ->subject('Test Subject')
             ->view('hello {{ $world }}')
             ->with(
                 [
                     'world' => 'earth',
                 ]
             );
    }
}
```

* Markdown template string

```php
use Smorken\String2Blade\Mail\Mailable;

class TestMarkdownMailable extends Mailable
{

    public function build(): void
    {
        $this->to('fiz@example.com')
             ->from('buz@example.com')
             ->subject('Test Subject')
             ->markdown($this->getMarkdown())
             ->with(
                 [
                     'world' => 'earth',
                 ]
             );
    }

    protected function getMarkdown(): string
    {
        return <<<STR
@component('mail::message')
# Hello

Your world is {{ \$world }}
@endcomponent
STR;
    }
}
```
