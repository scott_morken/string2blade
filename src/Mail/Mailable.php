<?php

namespace Smorken\String2Blade\Mail;

use Illuminate\Config\Repository as ConfigRepository;
use Illuminate\Container\Container;
use Smorken\String2Blade\Concerns\IsString2BladeTrait;
use Smorken\String2Blade\Support\ComponentFromString;

class Mailable extends \Illuminate\Mail\Mailable
{
    use IsString2BladeTrait;

    protected function buildView(): array|string|null
    {
        $this->ensureViewsFromStrings();

        return parent::buildView();
    }

    protected function ensureViewsFromStrings(): void
    {
        // @phpstan-ignore isset.property
        if (isset($this->view) && $this->isString2Blade($this->view)) {
            $this->view = $this->viewFromString($this->view);
        }

        // @phpstan-ignore isset.property
        if (isset($this->textView) && $this->isString2Blade($this->textView)) {
            $this->textView = $this->viewFromString($this->textView);
        }
    }

    protected function markdownRenderer()
    {
        return tap(Container::getInstance()->make(Markdown::class), function ($markdown) {
            $markdown->theme($this->theme ?: Container::getInstance()->get(ConfigRepository::class)->get(
                'mail.markdown.theme', 'default')
            );
        });
    }

    protected function viewFromString(string $template): string
    {
        $component = ComponentFromString::create($template);

        return $component->resolveView();
    }
}
