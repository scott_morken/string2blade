<?php

declare(strict_types=1);

namespace Smorken\String2Blade\Mail;

use Illuminate\Support\HtmlString;
use Illuminate\Support\Str;
use Smorken\String2Blade\Concerns\IsString2BladeTrait;
use Smorken\String2Blade\Support\ComponentFromString;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;

class Markdown extends \Illuminate\Mail\Markdown
{
    use IsString2BladeTrait;

    public function render($view, array $data = [], $inliner = null)
    {
        $this->view->flushFinderCache();

        $contents = $this->getContents($view, $data);

        if ($this->view->exists($customTheme = Str::start($this->theme, 'mail.'))) {
            $theme = $customTheme;
        } else {
            $theme = str_contains($this->theme, '::')
                ? $this->theme
                : 'mail::themes.'.$this->theme;
        }

        return new HtmlString(($inliner ?: new CssToInlineStyles)->convert(
            $contents, $this->view->make($theme, $data)->render()
        ));
    }

    /**
     * Render the Markdown template into text.
     *
     * @param  string  $view
     * @return \Illuminate\Support\HtmlString
     */
    public function renderText($view, array $data = [])
    {
        $this->view->flushFinderCache();

        $contents = $this->getContents($view, $data);

        return new HtmlString(
            html_entity_decode((string) preg_replace("/[\r\n]{2,}/", "\n\n", $contents), ENT_QUOTES, 'UTF-8')
        );
    }

    protected function getContents($view, array $data): string
    {
        if ($this->isString2Blade($view)) {
            $component = ComponentFromString::create($view);

            return $this->view->replaceNamespace(
                'mail', $this->htmlComponentPaths()
            )->make($component->resolveView(), $data)->render();
        }

        return $this->view->replaceNamespace(
            'mail', $this->htmlComponentPaths()
        )->make($view, $data)->render();
    }
}
