<?php

declare(strict_types=1);

namespace Smorken\String2Blade;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Smorken\String2Blade\Contracts\Support\ViewFromString;
use Smorken\String2Blade\Mail\Markdown;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        //
    }

    public function register(): void
    {
        $this->registerViewFromString();
        $this->registerMarkdownRenderer();
    }

    protected function registerMarkdownRenderer(): void
    {
        $this->app->singleton(Markdown::class, function (Application $app) {
            $config = $app->make('config');

            return new Markdown($app->make('view'), [
                'theme' => $config->get('mail.markdown.theme', 'default'),
                'paths' => $config->get('mail.markdown.paths', []),
            ]);
        });
    }

    protected function registerViewFromString(): void
    {
        $this->app->bind(ViewFromString::class, fn (Application $app) => new \Smorken\String2Blade\Support\ViewFromString($app[Factory::class]));
    }
}
