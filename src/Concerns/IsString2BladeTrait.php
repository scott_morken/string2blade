<?php

namespace Smorken\String2Blade\Concerns;

trait IsString2BladeTrait
{
    protected function isString2Blade($path): bool
    {
        return preg_match('/[@<>$ {]/im', (string) $path);
    }
}
