<?php

declare(strict_types=1);

namespace Smorken\String2Blade\Support;

use Illuminate\View\Component;

class ComponentFromString
{
    public static function create(string $template): Component
    {
        return new class($template) extends Component
        {
            public function __construct(protected string $template) {}

            public function render()
            {
                return $this->template;
            }
        };
    }
}
