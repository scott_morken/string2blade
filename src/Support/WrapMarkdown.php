<?php

declare(strict_types=1);

namespace Smorken\String2Blade\Support;

class WrapMarkdown
{
    public const END = '</x-mail::message>';

    public const START = '<x-mail::message>';

    public static function wrap(string $markdown): string
    {
        $markdown = trim($markdown);
        if (! str_starts_with(WrapMarkdown::START, $markdown)) {
            $markdown = WrapMarkdown::START.PHP_EOL.$markdown;
        }
        if (! str_ends_with(WrapMarkdown::END, $markdown)) {
            $markdown = $markdown.PHP_EOL.WrapMarkdown::END;
        }

        return $markdown;
    }
}
