<?php

declare(strict_types=1);

namespace Smorken\String2Blade\Support;

use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class ViewFromString implements \Smorken\String2Blade\Contracts\Support\ViewFromString
{
    public function __construct(protected Factory $factory) {}

    public function view(string $string, array $data = []): View
    {
        $component = ComponentFromString::create($string);

        return $this->factory->make($component->resolveView(), $data);
    }
}
