<?php

declare(strict_types=1);

namespace Smorken\String2Blade\Contracts\Support;

use Illuminate\Contracts\View\View;

interface ViewFromString
{
    public function view(string $string, array $data = []): View;
}
